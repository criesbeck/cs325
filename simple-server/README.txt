;;; Latest update: 09-18-2014

Simple Server
-------------

Simple Server provides a small common API on top of AllegroServe
and Hunchentoot.

JSON Demo
---------

Is a small example web app using Simple Server to demonstrate 
accessing static HTML assets and JSON communication between
JavaScript on the client and Lisp on the server. 

For more information, see

https://www.cs.northwestern.edu/academics/courses/325/readings/web-services.php