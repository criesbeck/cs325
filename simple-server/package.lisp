(defpackage #:simple-server
  (:use #:common-lisp)
  (:export 
   ;; for webapps
   #:get-route-map #:param-value #:stop-server
   
   ;; for server implementations
   #:get-response-function #:set-response-function #:set-server
   #:*port-servers* #:*root-dir*
      
   ;; must be defined by server implementations
   #:defroute #:start-server
   ))
