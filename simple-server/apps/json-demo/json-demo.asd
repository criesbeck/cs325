;;;; json-demo.asd

(asdf:defsystem #:json-demo
  :serial t
  :depends-on ("simple-server" "cl-json" "drakma")
  :components ((:file "json-demo")))

