;;; simple-server.lisp
;;;
;;; JSON server built on Simple Server
;;;
;;; Defines STOP-SERVER and PARAM-VALUE
;;; Needs server-specific implementations of DEFROUTE and START-SERVER

;;; Update history:
;;;
;;; 08-26-2015 Simplified SAVE-DATA example [CKR]

(defpackage #:json-demo
  (:use #:common-lisp #:json #:simple-server))

(in-package :json-demo)

;;; This demonstrates how to use SimpleServer and CL-JSON to implement a JSON-based web app.
;;;
;;; Key parts:
;;;   - static HTML5 files (HTML, CSS, JavaScript, images, ...)
;;;   - JavaScript in browser sends and gets JSON from the server, using AJAX
;;;   - Lisp code on server gets data from request parameters and JSON, and returns JSON
;;;
;;; The sample HTML file has has two toy forms.
;;;
;;; One form lets a user store a key-value pair on the server, and get back a JSON
;;; object with all stored key value pairs. This example illustrates sending data in JSON form.
;;;
;;; The other form lets a user send a string and a Lisp package name to the server, and get
;;; back a list of all symbols containing that string exported by the given package. This
;;; example sends data with request parameters, and gets a JSON array back.


;;; Publishing routes to JSON endpoints
;;; -----------------------------------


;;; POST /save-data?name=... + JSON data
;;;   Push the list equivalent of the JSON data in the global Lisp
;;;   variable name
;;;   Return the new list of stored values.

(defroute :post "/save-data" 'save-data)

(defvar *alist* nil)

(defun save-data (data params)
  (encode-json-alist-to-string (update-alist (get-json-from-string data))))

(defun get-json-from-string (str)
  (and str (stringp str) (> (length str) 0)
       (decode-json-from-string str)))

(defun update-alist (alist)
  (setq *alist*
        (remove-duplicates (append *alist* alist) :key 'car)))


;;; ------------------------------

;;; GET /exports?name=...&package=...
;;;   Return a JSON string with name, package, and the exported symbols
;;;   in package that contain name.

(defroute :get "/exports" 'exports)

(defun exports (data params)
  (let ((name (param-value "name" params ""))
        (package (param-value "package" params "COMMON-LISP")))
    (encode-json-plist-to-string 
     (list :name name :package package
           :symbols (exports-list name package)))))


(defun exports-list (filter package-name)
  (let ((package (find-package (string-upcase package-name)))
        (str (string-upcase filter))
        (symbols nil))
    (unless (null package)
      (do-external-symbols (sym package symbols)
        (when (search str (symbol-name sym))
          (push (symbol-name sym) symbols))))))

;;; --------------------------------

;;; get JSON from URL
;;;
;;;
;;; Drakma JSON example using a file stream to avoid an intermediate string,
;;; modified to call CL-JSON
;;; http://weitz.de/drakma/#ex-response-stream
;;;
;;; Example call: Firebase REST example 
;;;
;;;   (get-json-from-url "https://docs-examples.firebaseio.com/rest/quickstart/users.json")

(defun get-json-from-url (url)
  (let ((stream (drakma:http-request url :want-stream t)))
    (setf (flexi-streams:flexi-stream-external-format stream) :utf-8) 
    (json:decode-json stream)))