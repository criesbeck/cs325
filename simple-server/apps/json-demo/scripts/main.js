
// send a { key: value } object to /save-data for storage
export const saveData = (key, val) => (
  fetch('/save-data', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({ [key]: val }),
  })
);

// get a JSON with symbols in a Lisp package containing name
export const getExports = (name, lispPackage) => (
  fetch('/exports?' + new URLSearchParams(lispPackage ? { name, package: lispPackage } : { name }), {
    headers: {
      'Content-Type': 'application/json'
    }
  })
);
