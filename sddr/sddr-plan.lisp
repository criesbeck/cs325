(in-package :sddr-tests)

;;; Update history:
;;;
;;; 11-07-2021 removed DIFFERENT rules
;;; 11-03-2021 simplify PUSH-BOX and remove room locations from Monkey and bananas
;;; 11-03-2021 add NOT SAME to PLAN rules
;;; 11-10-2020 expanded commentary [CKR]
;;; 11-09-2020 renamed MB-STATE to STATE [CKR]
;;; 11-15-2018 removed unnecessary SUBSTACK rules [CKR]
;;; 10-14-2015 simplified walking in Monkey and Bananas [CKR]
;;; 10-13-2015 created file, based on DDR version [CKR]

;;; This is a basic framework for doing planning
;;; with the deductive retriever (DDR).
;;;
;;; To get a plan,
;;;
;;;   (ASK '(PLAN ?plan start-state goal-state) rule-base)
;;;
;;; The predicate (PLAN plan start-state goal-state) asserts
;;; that executing plan in a situation described start-state
;;; would lead to the situation described by goal-state.
;;;
;;; A plan is a list of actions, i.e., either NIL, for the
;;; empty list, or a functional term (CONS action actions) where
;;; action is some term you define to describe an action in
;;; a given problem domain, and actions is the rest of the plan.
;;;
;;; States are functional terms you define to describe a problem
;;; situation. More below.
;;;
;;; It turns out that just two rules can define planning in
;;; general.

(defparameter *plan-kb*
  '(
    ;; The empty plan works if current state = goal state
    (<- (plan nil ?goal ?goal))
    
    ;; Use the plan (cons action actions) if
    ;;   - current state is not goal state
    ;;   - action is a good step given the current and goal states
    ;;   - action leads to the result state given the current state
    ;;   - the remaining actions are a plan to get to the goal
    ;;     given that result state
    (<- (plan (cons ?action ?actions) ?current ?goal)
        (not (same ?current ?goal))
        (step ?action ?current ?goal)
        (results ?action ?current ?result)
        (plan ?actions ?result ?goal))
    
    (<- (same ?x ?x))
    ))

;;; Include these two rules in your planner. No other
;;; PLAN rules should be needed.

;;; To define a set of planner you need to decide what your
;;; actions and states are, how to represent them with functional
;;; terms, then write rules for STEP and RESULTS using those
;;; actions and states.
;;;
;;; A state is a term. Although it could be a symbol, like ON or OFF,
;;; most problems require more complex states using functional terms.
;;; For example, the state of a "river crossing puzzle" would describe
;;; the locations of the various animals that need to be ferried
;;; across a river.
;;;
;;; There should be just one way to describe any given state. Otherwise
;;; the number of rules you have to write will explode when you try
;;; to handle alternative ways of describing the same thing.
;;;
;;; Actions are usually pretty simple. An action might be a symbol,
;;; like TURN-ON or TURN-OFF, or a functional term, indicating an
;;; action applied to one or more objects, like (MOVE ?OBJ ?LOC)
;;; to describe moving an object to a location.
;;;
;;; Once you have defined your actions and states, you need to
;;; write rules describing how actions affect states.
;;;
;;; Use rules with (RESULTS action current-state next-state) to
;;; assert that doing action in current-state leads to next-state.
;;; For example, if your state terms include the locations of objects,
;;; then a MOVE action would lead to a next-state with an object
;;; in the location it was moved to.
;;;
;;; RESULTS rules are usually pretty simple. They might have an
;;; IF part to specify conditions that must hold for the action
;;; to be even possible, but many RESULTS rules have only a
;;; THEN part.
;;;
;;; Finally, you need to write rules to suggest what steps to
;;; explore when searching for a plan for a given goal state and
;;; current state. These rules are where the "intelligence" is.
;;; The rules must not only suggest reasonable rules, but avoid
;;; infinite loops.
;;;
;;; For example, you will get an infinite loop, if you have one rule
;;; that suggests turning on a lamp if it's off, and another rule
;;; that suggests turning off a lamp if it's on. The rules need
;;; to include what the goal state is, e.g., being able to read,
;;; or being able to sleep, so that only one rule applies.
;;;
;;; Use rules with (STEP action current-state goal-state) to assert
;;; that action is a good step in a plan to get from current-state
;;; to goal-state.
;;;
;;; Two examples follow.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Monkey and Bananas
;;;
;;; This is a classic commonsense reasoning AI problem.
;;;
;;; A monkey enters a room and sees bananas hanging out
;;; of reach in the center of the room. The monkey sees
;;; a box over by a window. The monkey figures out that
;;; she can walk to the box, push it to the center of
;;; the room, climb the box, and get the bananas.
;;;
;;; We want to design a deductive planner as smart as
;;; the monkey.
;;;
;;; We can represent possible states with
;;;
;;;   (STATE monkey-loc box-loc)
;;;
;;; We assume bananas are always in the center of the
;;; room. 
;;;
;;; A location can be DOOR, WINDOW or CENTER, or,
;;; BOX-TOP, if the monkey has climbed onto the box. The
;;; goal state therefore has the form
;;;
;;;   (STATE BOX-TOP CENTER)
;;;
;;; The query for a plan for when the monkey starts by the door,
;;; and the box is by the window is therefore 
;;;
;;;   (PLAN ?PLAN (STATE DOOR WINDOW) (STATE BOX-TOP CENTER))

;;; Here is one possible set of specific planning rules. 

(defparameter *monkey-kb*
  '(
    ;; ACTION RESULT RULES
    
    ;; climb-box changes the monkey's location from
    ;; a room location to the top of the box.
    ;; The monkey has to be at the box.
    (<- (results climb-box
                 (state ?bloc ?bloc)
                 (state box-top ?bloc)))
    
    ;; push-box changes the location of both the monkey
    ;; and the box.
    ;; The monkey has to be at the box.
    (<- (results (push-box ?bloc2)
                 (state ?bloc1 ?bloc1)
                 (state ?bloc2 ?bloc2)))
        
    ;; walk changes the location of the monkey.
    (<- (results (walk-to ?mloc2)
                 (state ?mloc1 ?bloc)
                 (state ?mloc2 ?bloc)))
    
    ;; STEP SELECTION RULES 
    ;;
    ;; These rules need to avoid endless loops and wasted
    ;; search.
    
    ;; Choose climb-box if box under bananas, and monkey not
    ;; not on the box already
    (<- (step climb-box
              (state ?gloc ?gloc)
              (state box-top ?gloc)))
    
    ;; Choose push-box to bananas if monkey at box, and
    ;; box not at bananas, 
    (<- (step (push-box ?gloc)
              (state ?bloc ?bloc)
              (state ?mloc ?gloc))
        (not (same ?bloc ?gloc)))
    
    ;; Choose walk to box if not at or on box
    (<- (step (walk-to ?bloc)
              (state ?mloc-1 ?bloc)
              (state ?mloc-2 ?gloc))
        (not (same ?mloc-1 ?bloc))
        (not (same ?mloc-1 box-top)))

    ))

;;; Here are some test cases, with the same goal, but different
;;; start states. Notice that we append the general planning rules
;;; with the specific planning rules.

(define-test monkey
    (let ((*rules* (append *plan-kb* *monkey-kb*)))
      (assert-true
       (ask '(plan ?plan
                   (state box-top center)
                   (state box-top center))
            *rules*))
      
      (assert-true
       (ask '(plan ?plan
                   (state center center)
                   (state box-top center))
            *rules*))
      
      (assert-true
       (ask '(plan ?plan 
                   (state window window)
                   (state box-top center))
            *rules*))

      (assert-true
       (ask '(plan ?plan
                   (state door window)
                   (state box-top center))
            *rules*))

      (assert-false
       (ask '(plan ?plan
                   (state box-top window)
                   (state box-top center))
            *rules*))
      ))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Blocks world, super-simplified
;;;
;;; Blocks world was a very early experimental domain for
;;; AI robotics and planning. A computer has a camera
;;; looking at children's blocks on a tabletop. It had
;;; an arm with a simple gripper that could pick up
;;; and move one block at a time. Blocks were stacked
;;; on the table (the current state) and the computer was
;;; to rearrange them into a different set of stacks (the
;;; goal state).
;;;
;;; We represent the state of the world as a list of
;;; blocks. E.g., to represent the stack with A on B on C:
;;;
;;;   (CONS A (CONS B (CONS C NIL))))
;;;
;;; The goal is to change from one stack to another,
;;; using a (minimal) sequence of POP and PUSH actions.
;;;
;;; The solution below uses *APPEND-KB* from sddr-tests.lisp.

(defparameter *blocks-world-kb*
  '(
    ;; ACTION RESULT RULES
    
    ;; POP removes the top block of the stack
    ;; (PUSH x) puts x on top of the stack
    
    (<- (results pop (cons ?x ?stack) ?stack))
    
    (<- (results (push ?x) ?stack (cons ?x ?stack)))
    
    ;; STEP SELECTION RULES 

    ;; Choose POP if the current stack isn't a substack
    ;; of the goal stack
    ;; Choose (PUT x) if x + current stack is a substack
    ;; of the goal stack
    
    (<- (step pop ?current ?goal)
        (not (append ?top ?current ?goal)))
        
    (<- (step (push ?x) ?current ?goal)
        (append ?top (cons ?x ?current) ?goal))
    ))

(define-test blocks-world
    (let ((*rules* (append *append-kb* *plan-kb* *blocks-world-kb*)))
      (assert-true
       (ask '(plan ?x nil (cons a nil)) *rules*))
      
      (assert-true
       (ask '(plan ?x (cons a nil) nil) *rules*))
      
      (assert-true
       (ask '(plan ?x (cons a nil) (cons b nil)) *rules*))
      
      (assert-true
       (ask '(plan ?x (cons b (cons a nil)) (cons a (cons b nil)))
            *rules*))
      
      (assert-true
       (ask '(plan ?x 
                   (cons b (cons c (cons a nil))) 
                   (cons c (cons b (cons a nil))))
            *rules*))
      ))
      
