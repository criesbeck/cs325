
;;; 11-09-2020 added sddr-plan [CKR]
;;; 09-17-2014 dropped clp-exts [CKR]

(asdf:defsystem #:sddr
  :serial t
  :depends-on ("lisp-unit")
  :components ((:file "sddr")
               (:file "sddr-tests")
               (:file "sddr-exs-tests")
               (:file "sddr-plan")))

