(in-package :sddr-tests)

(define-test blocks-world
  (let ((kb (append *plan-kb* *append-kb* *blocks-world-kb*)))
  
    (assert-true
     (ask '(plan ?x nil (cons a nil)) kb))
  
    (assert-true
     (ask '(plan ?x (cons a nil) nil) kb))
  
    (assert-true
     (ask '(plan ?x (cons a nil) (cons b nil)) kb))
  
    (assert-true
     (ask '(plan ?x (cons b (cons a nil)) (cons a (cons b nil))) kb))
  
    (assert-true
     (ask '(plan ?x 
                 (cons b (cons c (cons a nil))) 
                 (cons c (cons b (cons a nil))))
          kb))
    ))

(define-test monkey
  (let ((kb (append *plan-kb* *monkey-kb*)))
  
    (assert-true
     (ask '(plan ?plan
                 (state box-top center)
                 (state box-top center))
          kb))
  
    (assert-true
     (ask '(plan ?plan
                 (state center center)
                 (state box-top center))
          kb))
  
    (assert-true
     (ask '(plan ?plan 
                 (state window window)
                 (state box-top center))
          kb))
  
    (assert-true
     (ask '(plan ?plan
                 (state door window)
                 (state box-top center))
          kb))
  
    (assert-false
     (ask '(plan ?plan
                 (state box-top window)
                 (state box-top center))
          kb))
    ))
