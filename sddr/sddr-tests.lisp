;;; Change Log
;;;
;;; 11/10/2023: tweak *TIGER-KB* [CKR]
;;; 11/09/2020: removed outdated version of Monkey and Bananas [CKR]
;;; 11/03/2016: added Zhiping' Xiu's UNIFY test [CKR]
;;; 10/12/2015: addeds *BLOCKS-KB* example [CKR]
;;; 10/12/2015: added *MORTAL-KB* example [CKR]
;;; 10/07/2015: revised for the even simpler DDR [CKR]
;;; 01/31/2008: tests for simplified deductive retriever [CKR]


(defpackage #:sddr-tests
  (:use #:common-lisp #:lisp-unit #:sddr)
  )

(in-package :sddr-tests)

;;; Test unifier, since it can have subtle bugs

(define-test unify
  (assert-true (unify '?x '?x))
  (assert-true (unify '(?x a) '(a ?x)))
  (assert-false (unify '(?x b) '(a ?x)))
  (assert-true (unify '(a ?x) '(?x ?x)))
  (assert-true (unify '(?x ?y) '(?y a)))
  (assert-false (unify '?x '(f ?x)))
  (assert-false (unify '(f ?x) '?x))
  (assert-false (unify '(?x ?y) '(?y (f ?x))))
  (assert-true (unify '(?x a) '(?x ?x)))
  (assert-true (unify '?x '(a)))
  (assert-false (unify '(?y ?x) '((f ?x) ?y)))
  (assert-true (unify '(?x a) '((f ?y) ?y)))
  (assert-false (unify '(a ?x) '(b c)))
  (assert-true (unify '(?y ?x a) '(?x ?y ?x)))
  )

;;; Simple ancestry-type knowledge base.
;;;
;;; NOTE: This is a BAD set of rules. It's
;;; combinatorially explosive and produces 
;;; many redundant answers.
;;; E.g., try (ASK '(HORSE ?X) *HORSES-KB*).
;;; How can you fix these rules and still
;;; pass the tests?

(defparameter *horses-kb*
  '(
    ;; x is ancestor of y if x is a parent or a parent of an ancestor
    (<- (ancestor ?x ?y) (parent ?x ?y))
    (<- (ancestor ?x ?y) (parent ?x ?z) (ancestor ?z ?y))
      
    ;; x is a horse if x is a descendant of a horse
    (<- (horse ?x) (ancestor ?y ?x) (horse ?y))
      
    ;; some real horses, not all real relationships
    (<- (parent man-o-war war-admiral))
    (<- (parent war-admiral seabiscuit))
    (<- (parent seabiscuit kelso))
    (<- (horse man-o-war))
    ))

(define-test horses

  (dolist (x '(man-o-war war-admiral seabiscuit kelso))
    (assert-true (ask `(horse ,x) *horses-kb*)))

  (assert-equality set-equal
                   '((horse war-admiral) 
                     (horse seabiscuit) 
                     (horse kelso) 
                     (horse man-o-war))
                   (ask '(horse ?x) *horses-kb*))
  )


(defparameter *tiger-kb*
  '(
    (<- (eats antelope grass))
    (<- (eats antelope ferns))
    (<- (predator-of antelope tiger))
    (<- (predator-of zebra tiger))

    (<- (at antelope savannah))
    (<- (at tiger savannah))
    (<- (at grass savannah))
    (<- (at ferns savannah))
    (<- (at ferns jungle))

    (<- (can-survive ?x)
        (eats ?x ?y)
        (can-catch ?x ?y))

    (<- (eats ?y ?x)
        (predator-of ?x ?y))

    (<- (can-catch ?x ?y)
        (near ?x ?y))

    (<- (near ?x ?y)
        (at ?x ?loc)
        (at ?y ?loc))

    (<- (near ?x ?x))
    ))

(define-test tiger
  (assert-true (ask '(at antelope savannah) *tiger-kb*))
  (assert-true (ask '(near antelope grass) *tiger-kb*))
  (assert-true (ask '(can-catch antelope grass) *tiger-kb*))
  (assert-true (ask '(can-survive antelope) *tiger-kb*))
  (assert-true (ask '(can-survive tiger) *tiger-kb*))
  )

;;; All men are mortal...

(defparameter *mortal-kb*
  '(
    (<- (mortal ?x) (human ?x))
    (<- (human ?x) (taught ?y ?x) (human ?y))
    (<- (human socrates))
    (<- (taught socrates plato))
    ))

(define-test mortal
    (assert-true (ask '(human socrates) *mortal-kb*))
  (assert-true (ask '(mortal socrates) *mortal-kb*))
  (assert-true (ask '(human plato) *mortal-kb*))
  (assert-true (ask '(mortal plato) *mortal-kb*))
  (assert-equality set-equal
                   '((mortal plato) (mortal socrates))
                   (ask '(mortal ?x) *mortal-kb*))
  )

;;; blocks world
;;;
;;; tests fail until change to one of the rules.
;;; see http://www.cs.northwestern.edu/academics/courses/325/readings/deductive-retrieval.php#not

(defparameter *blocks-kb*
  '(
    (<- (block block1))
    (<- (block block2))
    (<- (on block1 block2))
    (<- (on block2 table))
    (<- (clear ?x) (not (on ?y ?x)))
    (<- (movable ?x) (block ?x) (clear ?x))
    ))

(define-test blocks
  (assert-true (ask '(clear block1) *blocks-kb*))
  (assert-false (ask '(clear block2) *blocks-kb*))
  (assert-true (ask '(movable block1) *blocks-kb*))
  (assert-false (ask '(movable block2) *blocks-kb*))
  (assert-equal '((movable block1)) (ask '(movable ?x) *blocks-kb*))
  (assert-equal '((clear block1)) (ask '(clear ?x) *blocks-kb*))
  )

;;; Addition, Peano style
;;;
;;; A simple example of using functional terms to represent
;;; constructed results.

(defparameter *peano-kb*
  '(
    (<- (add 0 ?x ?x))
    (<- (add (succ ?x) ?y (succ ?z))
        (add ?x ?y ?z))
    ))

(define-test peano
  ;; 0 + 0 = 0
  (assert-true (ask '(add 0 0 0) *peano-kb*))
  ;; 0 + 1 = 1
  (assert-true (ask '(add 0 (succ 0) (succ 0)) *peano-kb*))
  ;; 1 + 0 = 1
  (assert-true (ask '(add (succ 0) 0 (succ 0)) *peano-kb*))
  ;; 2 + 2 = 4
  (assert-true (ask '(add (succ (succ 0)) (succ (succ 0))
                          (succ (succ (succ (succ 0)))))
                    *peano-kb*))
  ;; 2 + 1 != 4
  (assert-false (ask '(add (succ (succ 0)) (succ 0)
                           (succ (succ (succ (succ 0)))))
                     *peano-kb*))
  ;; 0 + 0 => 0
  (assert-equal '((add 0 0 0)) (ask '(add 0 0 ?x) *peano-kb*))
  ;; 0 + 1 => 1
  (assert-equal '((add 0 (succ 0) (succ 0)))
                (ask '(add 0 (succ 0) ?x) *peano-kb*))
  ;; 1 + 0 => 1
  (assert-equal '((add (succ 0) 0 (succ 0)))
                (ask '(add (succ 0) 0 ?x) *peano-kb*))
  ;; 2 + 2 => 4
  (assert-equal '((add (succ (succ 0)) 
                       (succ (succ 0))
                       (succ (succ (succ (succ 0))))))
                (ask '(add (succ (succ 0)) (succ (succ 0)) ?x)
                     *peano-kb*))
  ;; 1 + x = 3 => x = 2, or 3 - 1 => 2
  (assert-equal '((add (succ 0) (succ (succ 0)) (succ (succ (succ 0)))))
                (ask '(add (succ 0) ?x (succ (succ (succ 0))))
                     *peano-kb*))
  ;; x + y = 3 => <3, 0>, <2, 1>, <1, 2>, <0, 3>
  (assert-equal '((add 0 (succ (succ (succ 0))) (succ (succ (succ 0))))
                  (add (succ 0) (succ (succ 0)) (succ (succ (succ 0))))
                  (add (succ (succ 0)) (succ 0) (succ (succ (succ 0))))
                  (add (succ (succ (succ 0))) 0 (succ (succ (succ 0))))
                  )
                (ask '(add ?x ?y (succ (succ (succ 0))))
                     *peano-kb*))
  )

;;; APPEND, Prolog-style

(defparameter *append-kb*
  '(
    (<- (append nil ?x ?x))
    (<- (append (cons ?x ?l1) ?l2 (cons ?x ?l3))
        (append ?l1 ?l2 ?l3))
    ))


(define-test append
  (assert-equal '((append (cons a (cons b nil))
                          (cons c nil)
                          (cons a (cons b (cons c nil)))))
                (ask '(append (cons a (cons b nil))
                              (cons c nil)
                              ?l)
                     *append-kb*))
  (assert-equal '((append (cons a (cons b nil))
                          (cons c nil)
                          (cons a (cons b (cons c nil)))))
                (ask '(append (cons a (cons b nil))
                              ?l
                              (cons a (cons b (cons c nil))))
                     *append-kb*))
  (assert-equal '((append nil (cons a (cons b nil)) (cons a (cons b nil)))
                  (append (cons a nil) (cons b nil) (cons a (cons b nil)))
                  (append (cons a (cons b nil)) nil (cons a (cons b nil))))
                (ask '(append ?x ?y (cons a (cons b nil)))
                     *append-kb*))
  )

;;; This checks for a variable renaming bug that was in 
;;; ddr.lisp for 20 years!

(defparameter *analogy-kb*
  '(
    (<- (analogous ?x ?y ?a ?b)
        (similar ?x ?a)
        (similar ?y ?b))
    (<- (similar ?x ?x))
    ))

(define-test analogy
  (assert-true (ask '(analogous a a a a) *analogy-kb* ))
  (assert-true (ask '(analogous b b b b) *analogy-kb*))
  (assert-true (ask '(analogous a b a b) *analogy-kb*))
  (assert-equal '((analogous a b a b))
                (ask '(analogous a b ?x ?y) *analogy-kb*))
  )
