(in-package :cl-user)

;;; Suggested start up file for CS 325
;;; Platform: LispWorks on Windows and MacOS

#-quicklisp
(eval-when (:compile-toplevel :load-toplevel :execute)
  (let ((quicklisp-init (merge-pathnames "quicklisp/setup.lisp" (user-homedir-pathname))))
    (if (probe-file quicklisp-init)
      (load quicklisp-init)
      (error "Could not find ~~/quicklisp/setup.lisp"))))

;;; If both fasl and lisp exist, load the newer one
(setq hcl:*load-fasl-or-lisp-file* :load-new-no-warn)

;;; Set the depth of trace output to a large but finite level.
(setq hcl:*trace-print-level* 10)

;;; Turn off backup files
(setf (editor:variable-value 'editor::backups-wanted) nil)

;;; If both fasl and lisp exist, load the newer one
(setq hcl:*load-fasl-or-lisp-file* :load-newer-no-warn)

;;; Tell editor to indent on return, use native editor key bindings
(editor:bind-key "Indent New Line" #\Return :global #+mac :mac #+os-windows :pc)

;;; add ASD to list of source Lisp files
(editor:define-file-type-hook 
	("lispworks" "lisp" "slisp" "l" "lsp" "mcl" "cl" "asd")
	(buffer type)
	(declare (ignore type))
	(setf (editor:buffer-major-mode buffer) "Lisp"))

;;; Load cs325.lisp to create the cs325 package.

(eval-when (:compile-toplevel :load-toplevel :execute)
  (format mp:*background-standard-output* "test output")
  (ql:quickload "cs325")
  (capi:interactive-pane-execute-command
   (capi:editor-pane
    (capi:find-interface 'lw-tools:listener))
   "(in-package :cs325-user)"))

  
