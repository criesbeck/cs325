This is a library of Lisp tools for [CS325 Introduction
to AI Programming](https://courses.cs.northwestern.edu/325/).

# Installing

Install the CS325 library 
as [a Quicklisp local project](https://www.quicklisp.org/beta/faq.html#local-project).

<u>In a Unix-style terminal command shell,</u> (MacOS, Linux, Git Bash on Windows)

```
cd ~/quicklisp/local-projects
git clone https://gitlab.com/criesbeck/cs325.git
```

<u>In Lisp,</u> tell Quicklisp to update its table of local projects.
    
```
(ql:register-local-projects)
```
Verify you can now load the 325 code.

```
(ql:quickload "cs325")
```

If everything is set up correctly, a number of files will be compiled and loaded. Compiling
happens only when a file is added or changed. Future loads will go more quickly.
 
You will see warnings about some undefined functions. These are functions you will define
in exercises. If you see any messages labeled **ERROR**, stop and check the
messages to see if you can determine the problem.

# Updating

Use git to update your CS325 library when there's an announcement that 
the source has changed. Open a terminal command shell and do the following:

```
cd ~/quicklisp/local-projects/cs325
git pull
```

When you re-start Lisp, Quicklisp will automatically 
recompile and load any updated code. You won't need to re-register
local projects unless a new project has been added.