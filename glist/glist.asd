(asdf:defsystem #:glist
  :serial t
  :depends-on ("lisp-unit")
  :components ((:file "glist")
               (:file "glist-tests")
               (:file "glist-exs-tests")))