(in-package :match-tests)

;;; run the tests for the baseline matcher

(defun test-matcher ()
  (run-tests match-? match-var match-* match-?? match-and))

(define-test match-? 
  (assert-true (match-p '? 'a))
  (assert-true (match-p 'a 'a))
  (assert-false (match-p 'b 'a))
  (assert-true (match-p '() '()))
  (assert-true (match-p '(?) '(a)))
  (assert-false (match-p '(?) '(a b)))
  (assert-true (match-p '? '(a b)))
  (assert-true (match-p '(? b ?) '(a b c)))
  (assert-false (match-p '((?)) '((a b))))
  (assert-true (match-p '((? ?)) '((a b))))
  (assert-false (match-p '(? ?) '((a b))))
  (assert-false (match-p '(?) '()))
  (assert-true (match-p '? '()))
  (assert-false (match-p '(?) 'a))
  )

(define-test match-var  
  (assert-equal '(((?x . a))) (match-p '?x 'a))
  (assert-equal '(((?x . nil))) (match-p '?x nil))
  (assert-false (match-p '(?x ?x) '(a b)))
  (assert-equality set-equal '(((?y . a) (?x . a))) (match-p '(?x ?y) '(a a)))
  (assert-equality set-equal '(((?y . b) (?x . a))) (match-p '(?x ?y) '(a b)))
  )

(define-test match-*
  (assert-equal '(((?x . a))) (match-p '(?* ?x) '(a)))
  (assert-equal '(((?x . d))) (match-p '(?* ?x) '(a b c d)))
  (assert-equal '(nil) (match-p '(?*) '(a b c d)))
  (assert-equal '(nil) (match-p '(?*) '()))
  (assert-false (match-p '(?* ?x) '()))
  (assert-equality set-equal '(((?x . a)) ((?x . b)) ((?x . c))) (match-p '(?* ?x ?*) '(a b c)))
  )

(define-test match-??
  (assert-equal '(nil)  (match-p '(?? numberp) '12))
  (assert-false  (match-p '(?? numberp) 'a))
  (assert-equal '(nil)  (match-p '(?? stringp) "hello"))
  (assert-equal '(nil)  (match-p '(?? > 5) 12))
  (assert-false  (match-p '(?? > 5) 3))
  )

(define-test match-and
  (assert-equal '(nil) (match-p '(?and) nil))
  (assert-equal '(((?x . 12)))  (match-p '(?and (?? numberp) ?x) 12))
  (assert-false  (match-p '(?and (?? > 15) ?x) 12))
  
  (assert-equality set-equal '(((?x . 24)) ((?x . 30)))  (match-p '(?* (?and (?? > 20) ?x) ?*) '(24 11 3 30)))
  )

(define-test match-not
  (assert-false (match-p '(?not (a b c)) '(a b c)))
  (assert-equal '(nil) (match-p '(?not (a b c)) 12))
  (assert-false (match-p '(?not ?x) 'a))
  (assert-equal '(((?x . a))) (match-p '(?x (?not ?x)) '(a b)))
  (assert-equal '(nil) (match-p '(?not (?? > 5)) 1))
  (assert-false (match-p '(?not (?? > 5)) 8))
  
  (assert-equal '(((?x . 1))) (match-p '(?and (?not (?? > 5)) ?x) 1))
  (assert-false (match-p '(?and (?not (?? > 5)) ?x) 8))
  (assert-equal '(nil) (match-p '(?not (?and ?x (?? numberp))) 'a))
  
  (assert-equal '(((?y . a) (?x . a))) 
                (match-p '(?x (?or ?x ?y) (?not ?y)) '(a a c)))

  (assert-equality set-equal
     '(((?y . a) (?x . a)) ((?z . a) (?y . a) (?x . a))) 
     (match-p '(?x (?or ?x ?y) (?or ?x ?z) (?not ?y)) '(a a a c)))
  )

(define-test match-or
  (assert-equal '(nil) (match-p '(?or a b) 'a))
  (assert-equal '(nil) (match-p '(?or a b) 'b))
  (assert-false (match-p '(?or a b) 'c))
  (assert-false (match-p '(?or a b) nil))
  
  (assert-equality set-equal '(((?x . a)) ((?y . a))) (match-p '(?or ?x ?y) 'a))
  (assert-equality set-equal '(((?y . b) (?x . a)) ((?z . a) (?y . b) (?x . a))) 
                (match-p '(?x ?y (?or ?x ?y ?z)) '(a b a)))
  
  (assert-equal '(nil) (match-p '(?or (?? > 5) (?? < 0)) 10))
  (assert-equal '(nil) (match-p '(?or (?? > 5) (?? < 0)) -3))
  (assert-false (match-p '(?or (?? > 5) (?? < 0)) 3))
  
  (assert-equal '(((?x . 1))) (match-p '(?and (?or (?? < -10) (?? > 0)) ?x) 1))
  )

(define-test match-=
  (assert-equal '(nil) (match-p '(?= 9 square) 3))
  (assert-false (match-p '(?= 9 square) 2))
  (assert-equal '(nil) (match-p '(?= 6 + 1 2) 3))
  (assert-equal '(nil) (match-p '(?= (?not nil) contains "foo") "some food"))
  (assert-equal '(((?x . 9))) (match-p '(?= ?x square) 3))
  (assert-equal '(((?x . 9))) (match-p '((?= ?x square) ?x) '(3 9)))
  (assert-false (match-p '((?= ?x square) ?x) '(3 6)))
  (assert-true (match-p '(?x (?= ?x square)) '(9 3)))
  (assert-false (match-p '(?x (?= ?x square)) '(6 3)))
  )

(defun square (x) (* x x))
(defun contains (x y) (and (stringp x) (search y x)))


(define-test match-contains
  (assert-equal '(((?x . a))) (match-p '(?contains ?x) 'a))
  (assert-equality set-equal '(((?x . ((a)))) ((?x . (a))) ((?X . a)))
                   (match-p '(?contains ?x) '((a))))
  (assert-equal '(((?x . nil))) (match-p '(?contains ?x) nil))
  (assert-equality set-equal '(((?x . (nil))) ((?x . nil)))
                   (match-p '(?contains ?x) '(nil)))
  (assert-true (match-p '(?x (?contains ?x)) '(2 (1 2 3))))
  (assert-false (match-p '(?x (?contains ?x)) '(4 (1 2 3))))
  (assert-equality set-equal '(((?x . (a b))) ((?x . b)) ((?x . a)))
     (match-p '(?contains ?x) '(a b)))
  (assert-equality set-equal 
                   '(((?x . (a b c))) ((?x . a)) ((?x . b)) ((?x . c))) 
                   (match-p '(?contains ?x) '(a b c)))
  (assert-equality set-equal '(((?x . 5)) ((?x . 12)))
     (match-p '(?contains (?and (?? numberp) ?x)) '((a 12) c (((5))))))
  (assert-equality set-equal 
                  '(((?x . (a (b c)))) ((?x . a)) ((?x . (b c))) ((?x . b)) ((?x . c)))                  
                   (match-p '(?contains ?x) '(a (b c))))
  )
