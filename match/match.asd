(asdf:defsystem #:match
  :serial t
  :depends-on ("lisp-unit")
  :components ((:file "package")
               (:file "match")
               (:file "match-tests")))