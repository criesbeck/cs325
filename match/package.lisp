(defpackage #:exmatch
  (:use #:common-lisp)
  (:export #:match-p #:? #:?* #:?? #:?= #:?and #:?or #:?not #:?contains))

(defpackage #:match-tests
  (:use #:common-lisp #:lisp-unit #:exmatch))
