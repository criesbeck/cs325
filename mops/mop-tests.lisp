(defpackage #:mop-tests
  (:use #:common-lisp #:lisp-unit #:mops))

(in-package #:mop-tests)

(defconstant *mop-file*
  (merge-pathnames "~/quicklisp/local-projects/cs325/mops/mop-examples.txt"))

;;; Existing MOP functions

(define-test isa-p
  (unless (kb-source)
    (load-kb *mop-file*))
  (assert-true (isa-p 'clyde-1 'animal))
  (assert-true (isa-p 'buddy 'animal))
  (assert-true (isa-p 'buddy 'small-thing))
  (assert-false (isa-p 'clyde-1 'chihuahua))
  (assert-false (isa-p 'elephant 'clyde-1))
  )

(define-test get-filler
  (unless (kb-source)
    (load-kb *mop-file*))
  (assert-equal 'white (get-filler 'clyde-1 'color))
  (assert-equal 'gray (get-filler 'elephant 'color))
  (assert-equal nil (get-filler 'willy 'can-fly))
  (assert-equal t (get-filler 'willy 'can-swim))
  (assert-equal t (get-filler 'tweety 'can-fly))
  (assert-equal nil (get-filler 'tweety 'can-swim))
  )

;;; Tests for exercises

(define-test has-slots-p
  (unless (kb-source)
    (load-kb *mop-file*))
  (assert-true (has-slots-p 'jumbo-1 '((color gray) (name "Jumbo"))))
  (assert-false (has-slots-p 'clyde-1 '((color gray))))
  (assert-true (has-slots-p 'event-1 '((actor elephant) (action ingest))))
  (assert-true (has-slots-p 'event-1 '((actor elephant))))
  (assert-false (has-slots-p 'event-1 '((actor pig) (action ingest))))
  (assert-true (has-slots-p 'buddy '((brain nil))))
  (assert-false (has-slots-p 'tweety '((brain nil))))
  )

(define-test pedalo
  ;;; always reload mops to force re-linearization
  (load-kb *mop-file*)
  (assert-equal '5 (get-filler 'pedal-wheel-boat 'navzone))
  (assert-equal '5 (get-filler 'small-catamaran 'navzone))
  (assert-equal '5 (get-filler 'pedalo 'navzone))
  )  
 
(define-test mop-search
  (unless (kb-source)
    (load-kb *mop-file*))
  (assert-equal '(canary) (mop-search 'bird '((color yellow))))
  (assert-equal '(tweety) (mop-search 'bird '((owner granny))))
  (assert-equal '(tweety) (mop-search 'bird '((color yellow) (owner granny))))
  (assert-equal '(buddy) (mop-search 'animal '((age 7))))
  (assert-equal '(chihuahua) (mop-search 'animal '((brain nil))))
  
  (assert-equal nil (mop-search 'event '((actor elephant))))
  (assert-equal '(event-1) (mop-search 'event '((actor clyde-1))))
  )

(defun init-dmap ()
  (unless (kb-source)
    (load-kb *mop-file*))
  (set-phrases 
   '((clyde-1 clyde)
     (peanuts peanuts) 
     (ingest-event (actor) ate (object))
     (human someone)
     (economist economist)
     (milton-friedman milton friedman)
     (interest-rates interest rates)
     (increase rising)
     (decrease falling)
     (change-event (variable) are (direction))
     (communication-event (actor) said (object))
     )))

(defun includes (lst1 lst2)
  (every (lambda (x) (member x lst2)) lst1))

(define-test dmap
  (init-dmap)

  ;;; a simple word goes to concept
  (assert-equal '(clyde-1) (references (dmap '(clyde))))
  ;;; a multi-word name goes to concept
  (assert-equal '(milton-friedman) (references (dmap '(milton friedman))))
  ;;; a word going to a concept with same name does not loop
  (assert-equal '(peanuts) (references (dmap '(peanuts))))
  ;;; a sentence goes to an event instance with given slots
  (assert-equality includes '(clyde-1 peanuts event-1)
                   (references (dmap '(clyde ate some peanuts))))
  ;;; a sentence about an event containing an event does not loop
  (assert-equality includes '(milton-friedman interest-rates-rise friedman-said-event-1)
                   (references (dmap '(milton friedman said interest rates are rising))))
  )
