(defsystem mops
  :serial t
  :depends-on ("lisp-unit")
  :components ((:file "mops")
               (:file "mop-tests")))